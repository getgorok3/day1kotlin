//Hello World
fun main(arg: Array<String>) {
    val x: Int = 1
    val y: Int = 2
    val z = x + y         //find matched data type automatically  but if z = int(int + int), z will be int forever
    println("Hello, World! $z")     // String format easy to use $Val
    // Nullable
    val data: Int
    val dataNulable: Int?       // check null prevent null pointer exception
    data = 2
    println(y == data)
    println(y === data)
//    dataNulable = null
//    data = dataNulable        //can not reassigned
    helloWorld()
    helloWorld("Prayuth")
    helloWorld("Sanchai")
    showName("Thanakan", "Thanakwang")
    showName()

    gradeReport("Somchai", 3.33)
    gradeReport()
    gradeReport(name = "Nobita")
    gradeReport(gpa = 2.50)

    println(isOdd(3))
    println(isOdd(4))

//    println( getAbbreviation('A'))
//    println( getAbbreviation('B'))
//    println( getAbbreviation('C'))
//    println( getAbbreviation('F'))

    println(showGradeResult(89))
    println(showGradeResult(73))
    println(showGradeResult(55))
    println(showGradeResult(43))
    println(showGradeResult(120))



    //loop
    for(i in 1..3){
        println("loop $i")
    }
    //down to
    for(i in 6 downTo 0 step  2){
        println(" each minus 2: $i")
    }
    // arrays
    val arrays = arrayOf(1,3,5,7)
    for(i in arrays.indices){
        println("indices "+arrays[i])
    }
    for(( index,value) in arrays.withIndex()) {         // can matched index with value
        println("$index . value = $value")
    }

    var arrays2 = arrayOf(5,55,200,1,3,5,7)
    var max =findMaxValue(arrays2)
    println("max value is $max")
}

fun helloWorld(): Unit {
    println("Hello World! ")
}

fun helloWorld(text: String): Unit {
    println("Hello $text")
}

fun showName(name: String = "Defuakt name", surname: String = "Defualt surname"): Unit =
    println("surname: $surname name: $name ")


fun gradeReport(name: String = "annonymous", gpa: Double = 0.00): Unit = println("mister $name gpa: is $gpa ")
// shortCut fun


fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'F' -> return "Fantastic"
        'C' -> {
            println(" not smart")
            return "cheap"
        }
        else -> return " Hello"
    }
}

fun getGrade(score: Int): String? {
    val grade: String?
    when (score) {
        in 0..50 -> grade = "F"                     // when = switch , in rang
        in 51..70 -> grade = "C"
        in 70..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun showGradeResult(score: Int): String? {
    val grade = getGrade(score)
    val result: String?
    when (grade) {              // prevent null pointer
        null -> result = "Wrong score input"
        else ->  result = "score: $score = " + getAbbreviation(grade.toCharArray()[0])
    }
    return result
}

fun findMaxValue(values: Array<Int>): Int{
    var max :Int = values[0]
    for( i in values){
        if(i > max){
            max = i
        }
    }
    return max
}

