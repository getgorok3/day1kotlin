abstract class Animal(var noOfLegs: Int, var food: String) {

    abstract fun getSound(): String
}

fun main(array: Array<String>) {
    val dog: Dog = Dog(4, "pedigree", "Sammy")
    val lion: Lion = Lion(4, "Meat")
    println(dog.getSound())
    println(lion.getSound())

    val teacher1: Teacher = Teacher("Somsak","Democratic", Color.GREEN)
    val teacher2: Teacher =Teacher("Somsak","Democratic",Color.RED)
    val teacher3: Teacher =Teacher("Somsak", "Mathmatics", Color.BLUE)
    //Destructuring Declaration
    val (teacherName, teacherCourseName) = teacher3
    println("$teacherName teachers $teacherCourseName")

    //toString
    println("ToString $teacher1")
    println("ToString $teacher2")
    println("ToString $teacher3")

    //equals t f
    println(teacher1.equals(teacher3))
    println(teacher1.equals(teacher2))

    //getter
    println("getter "+teacher1.courseName)
    println("getter "+teacher1.name   )

    //component N
    println("component 1 "+teacher1.component1())
    println("component 2 "+teacher1.component2())

    //adhoc object no class
    var adHoc = object{
        var x: Int=0
        var y: Int=1
    }
    println(adHoc)
    println(adHoc.x+adHoc.y)
}

class Dog(noOfLegs: Int, food: String, var name: String) : Animal(noOfLegs, food) {
    override fun getSound(): String {
        return "$name Bark Hong Hong!!!"
    }
}

class Lion(noOfLegs: Int, food: String) : Animal(noOfLegs, food) {
    override fun getSound(): String {
        return "I'm Lion KOooooooooooooo!!!"
    }
}

data class Teacher(var name: String, var courseName: String, var shirtColor: Color){

}

//Enum explain status
enum class Color{
    RED,GREEN,BLUE
}
