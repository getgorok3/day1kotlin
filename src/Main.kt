//open class Person(var name: String, var surname: String, var gpa: Double) {
//    constructor(name: String, surname: String) : this(name, surname, 0.0) {
//    }
//
//    open fun getDetail(): String {
//        return "$name $surname has score $gpa"
//    }
//
//}
// open allow to override
abstract class Person(var name: String, var surname: String, var gpa: Double) {
    constructor(name: String, surname: String) : this(name, surname, 0.0) {
    }

    abstract  fun goodBoy(): Boolean
    open fun getDetail(): String {
        return "$name $surname has score $gpa"
    }
}

fun main(arg: Array<String>) {
//    val nol: Person = Person("somchai", "Pitak", 2.00)
//    val nol2: Person = Person("Prayu", "Son")
//    val nol3: Person = Person(surname ="Prayu", name ="Son")
//    println(nol)
//    println(nol.name)
//    println(nol.getDetail())
//    println(nol2.getDetail())
//    println(nol3.getDetail())

    val nolStudent : Student  = Student("Somchai", "Pitak", 2.00, "Camt")
    println(nolStudent.getDetail())
}

class Student(name: String, surname: String, gpa: Double, var department: String): Person(name,surname,gpa){
    override fun getDetail(): String {
//        return "$name $surname has score $gpa and study in $department"
        return super.getDetail() + "and study in $department"
    }

    override fun goodBoy(): Boolean {
        return gpa > 2.0
    }
}

